Tasks to answer in your own README.md that you submit on Canvas:

LINK TO REPOSITORY: https://bitbucket.org/garito456/exceptionrunner/ 

1.  See logger.log, why is it different from the log to console?

log prints more information than console does and also outputs the logs to a separate file.

2.  Where does this line come from? FINER org.junit.jupiter.engine.execution.ConditionEvaluator logResult Evaluation of condition [org.junit.jupiter.engine.extension.DisabledCondition] resulted in: ConditionEvaluationResult [enabled = true, reason = '@Disabled is not present']

This line comes from the .classpath file.

3.  What does Assertions.assertThrows do?

Checks to be sure an exception is thrown and if it is, it returns true, otherwise returns false.

4.  See TimerException and there are 3 questions
    1.  What is serialVersionUID and why do we need it? (please read on Internet)

	This attribute remembers the versions of the Serializable class to be sure that the loaded class and serialized object can work together.

    2.  Why do we need to override constructors?

	Created exceptions need to have a defined constructor, so by writing the constructor and passing it off to the super class, the exception is defined.

    3.  Why we did not override other Exception methods?

	There is no need to override the other Exception methods because they behave in the same way.

5.  The Timer.java has a static block static {}, what does it do? (determine when called by debugger)

It makes the code in that block only execute once. This is helpful for logging.

6.  What is README.md file format how is it related to bitbucket? (https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)

The README.md has Markdown syntax and is related to bitbucket because bitbucket displays the README.md when a repository is opened.

7.  Why is the test failing? what do we need to change in Timer? (fix that all tests pass and describe the issue)

The test is failing because a NullPointerException is being thrown instead of the expected TimerException. 

8.  What is the actual issue here, what is the sequence of Exceptions and handlers (debug)

The issue is that the TimerException class is of the NullPointerException, so when it is thrown, a NullPointerException is read.

9.  Make a printScreen of your eclipse JUnit5 plugin run (JUnit window at the bottom panel) 
10.  Make a printScreen of your eclipse Maven test run, with console
11.  What category of Exceptions is TimerException and what is NullPointerException

TimerException is of the NullPointerException and NullPointerException is of the general exception class.

12.  Push the updated/fixed source code to your own repository.